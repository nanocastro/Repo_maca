**Monitoreo AQ - Estado del arte**

** **

**A nivel global**

 

Los humanos manufacturamos el aire que respiramos de manera creciente, especialmente en las ciudades (Fenger, 2009). Como ciudadanos, vivimos inmersos en una atmósfera compleja y nociva que es el producto de un frenético metabolismo urbano y representa en la actualidad una de las mayores amenazas hacia la salud a nivel mundial (WHO, 2016). 

 

El monitoreo atmosférico ha sido clave en el descubrimiento, caracterización y actualización de los distintos fenómenos asociados a la contaminación del aire (Finlayson-Pitts, 2010) y es un aspecto fundamental de todos los estándares de calidad del aire establecidos para mitigar sus efectos (Jacobson, 2002). En general, se considera una práctica normal que sean solo los expertos al servicio de un organismo científico o ambiental público quienes tengan a su cargo la operación de costosas y complejas redes de monitoreo para generar y analizar datos acerca del estado del aire que respiramos. Además, sus informes han guiado, y convalidado, la planificación y el accionar de los tomadores de decisión encargados de intervenir en la regulación y la gestión de la distintos aspectos que afectan tanto la calidad del aire como la calidad de vida urbana (ie. transporte, salud, usos del suelo). 

 

Sin embargo, esta práctica de monitoreo ambiental y asesoría experta parece estar en crisis y en transición hacia un nuevo paradigma más abierto e inclusivo, no solo por sus limitaciones técnicas (baja resolución espacial, altos costos, etc) (Mead et al., 2013; Kumar et al., 2015) y las objeciones al rol público de esta ciencia normal (Funtowicz & Ravetz, 2003; Guimaraes et al, 2009; Albagi, 2015), sino también por las posibilidades derivadas de la masificación de las tecnologías de la información en general y de la reciente proliferación de sensores de bajo costo (Snyder et al., 2013). Amateurs, makers, hackers, comunidades de afectadas y ciudadanos científicos están utilizando y compartiendo los diseños de equipos científicos abiertos DIY (do it yourself) cambiando no sólo cómo y por quien son obtenidos los datos, sino también por qué, para qué y cómo los mismos están siendo presentados, y accionados, en la esfera pública (Dye, n.d.). Algunos ejemplos de esta tendencia se pueden encontrar en el desarrollo de equipos como: AirQualityEgg, PublicLab, SmartCitizen, AirCasting.  

 

Este cambio de paradigma presenta nuevos desafíos y oportunidades para el monitoreo de la calidad del aire.  Por un lado, existe cierto optimismo en cuanto a las oportunidades técnicas, y políticas, abiertas por este tipo de monitores de bajo costo y la mayor participación ciudadana, pero por otro lado, existe una fuerte preocupación acerca de la calidad de los datos generados. En este sentido se pueden mencionar los esfuerzos de las agencias ambientales y la academia por anticipar, incorporar, acompañar y dirigir los cambios producidos por esta ciencia comunitaria y ciudadana en sus propios roles y actividades (Spinelle et al, 2013; EPA, (n.d); CAP, 2015; WHO, 2017). En la actualidad, existe consenso en que este tipo de mediciones ciudadanas/comunitarias no pueden reemplazar las regulatorias sino que serían complementarias (Clements et al., 2017, Rai et al., 2017). Sin embargo, se hace énfasis en que para distintos tipos de datos pueden existir diferentes tipo de uso y que, a pesar de una menor calidad de los datos, estos pueden ser lo suficientemente buenos para un determinado fin, en especial para las comunidades afectadas (Gabrys et al., 2016). En la legislación europea (Air Quality Directive 2008/50/EC) se reserva la denominación de mediciones indicativas para aquellas que cumplan con unos determinados objetivos de calidad (DQO - data quality objectives) y  la calibración y la evaluación de la performance de estos sensores low-cost se ha tornado en un tema central al cual se están dedicando numerosas discusiones, investigaciones, prácticas y contestaciones (EPA, 2018; AQ-SPEC, (n.d.); Gabrys et al., 2018; Castell et al., 2017). 

 

La calibración se entiende como el proceso por el cual la respuesta de los sensores low-cost es comparada y ajustada a los datos de sensores de referencia.  Este proceso no es trivial dadas las características de los sensores (no linealidad, interferencia de temperatura y humedad, drift línea base, etc) y es una tendencia creciente aplicar regresiones multivariable, técnicas de aprendizaje de máquinas y otros complejos algoritmos que ayuden a mejorar la calidad de los datos. Usualmente ambos sensores (low-cost y referencia) son colocalizados en un ambiente representativo de las condiciones de muestreo y se utiliza este periodo como entrenamiento para desarrollar el algoritmo de ajuste. Luego, se utiliza este algoritmo para corregir la respuesta del sensor en distintos ambientes. En la actualidad, el uso  de aprendizaje de máquinas para calibración de sensores de bajo costo es un enfoque prometedor. La flexibilidad que ofrecen las herramientas de aprendizaje de máquinas hacen que este enfoque resulte atractivo, comparado con técnicas paramétricas que requieren la suposición de modelos paramétricos estáticos. Este tipo de técnicas poseen la habilidad de aprender relaciones entre las variables de interés de un problema, utilizando un conjunto de datos de entrenamiento. Respecto del uso de este tipo de técnicas para la calibración de sensores de calidad de aire de bajo costo, hasta la fecha se han publicado diversos estudios en los que se utilizan modelos aprendidos con diversas técnicas, como regresiones múltiples, redes neuronales, entre otras (Esposito et al., 2016; Spinelle et al., 2015, 2017, De Vito et al., 2008, 2009).

 

En las comunidades de práctica, tanto expertas como ciudadanas, existe el consenso de que es necesario poner en común capacidades y conocimientos para afrontar y resolver las limitaciones de los sensores de bajo costo y poder avanzar en acciones concretas, guiadas por los datos, para mejorar la salud pública en general. En este sentido, es necesario empoderar a los ciudadanos haciendo disponibles no solo las herramientas de sensado como ya ocurre (DIY/open source scientific hardware), sino también atender a sus limitaciones en cuanto al manejo de los datos, su análisis y su visualización (Clements et al., 2017). Herramientas abiertas de visualización como OpenAir (Carlslaw y Ropkins, 2012) y plataformas que permiten almacenar, descargar y visualizar datos como Luftdaten y Hackair son fundamentales para construir sentido (making sense) a partir de los datos y poder integrarlos en las historias particulares y los fines de los distintos usuarios (ie. data stories) (Gabrys et al., 2016). Poder evaluar, mejorar y expresar la calidad de los datos debería ser parte indispensable en la construcción de estos relatos con datos. Sin embargo, a pesar de la gran cantidad de publicaciones respecto a las calibraciones, los métodos y algoritmos de ajuste no son open source y por el contrario, parecen ser el nuevo foco de creación de valor de los sensores low-cost y en algunos casos ya están siendo patentados y comercializados (Zimmerman et al., 2018; Agilaire, n.d.). 

 

**A nivel local (Argentina)**

 

En Latinoamérica existe una falta crítica de datos de calidad del aire con algunas contadas excepciones en México, Brasil, Colombia y Chile (CAI, 2013; WB, 2017). Tanto en Argentina como otros paìses de Latam se han establecido estándares de calidad del aire, en su mayor parte copiados/adaptados de leyes y guías extranjeras (WHO, EPA), sin embargo, los protocolos, técnicas e instrumentos de monitoreo con los que dichas normas son evaluadas no han podido ser adaptados ni apropiados por los organismos de control locales debido a sus altos costos y a las dificultades técnicas que plantea su puesta en funcionamiento, resultando en un monitoreo de la calidad del aire históricamente insuficiente, y en muchos casos inexistente. En ciudades como Córdoba y Mendoza se han hecho esfuerzos para adquirir sofisticado instrumental de monitoreo automático extranjero, sin embargo, la falta de previsión respecto a los requerimientos técnicos y económicos de operación, mantenimiento y, especialmente, de calibración ha hecho que estos equipos no se hayan usado de manera sistemática. Aun cuando los repuestos y los gases patrón puedan ser conseguidos en el extranjero, la experiencia técnica local en la reparación y calibración es, no solo costosa, sino deficiente. 

 

Un contexto donde la cuestión ambiental es relegada sumado a las dificultades para acceder a la información pública y las características inherentes de la contaminaciòn del aire urbano (ie. invisible, interterritorial, etc) refuerzan y cierran el círculo vicioso por el que la calidad del aire es mantenida ausente en la esfera pùblica. Solo en casos puntuales donde ha habido una clara definición de contaminadores y afectados la contaminación del aire ha tomado estado público a partir de un conflicto y ha tenido efectos territoriales, jurídicos e institucionales (Murgida, 2014; Merlinsky , 2014). Algunos ejemplos de estos conflictos son los de las poblaciones cercanas a los polos petroquímicos de Dock Sud (dentro de la cuenca Matanza-Riachuelo), Bahía Blanca y Luján de Cuyo, y los de la ciudad de Puerto Madryn con la planta de aluminio de Aluar (Puliafito et al., 2013).

 

La ciencia local, a pesar de su amplia producción sobre contaminación atmosférica (Puliafito et al., 2013), tampoco ha contribuido a revertir la ausencia de esta problemática en foros públicos donde se evalúan y definen las distintas políticas públicas (transporte, salud, usos del suelo) que afectan tanto la calidad del aire como la calidad de vida urbana. A esto debe sumarse la poca llegada de la producción científica a los ciudadanos que pudieran estar interesados. Estas situaciones a su vez han redundado en un limitado interés para el financiamiento y la ampliación de los estudios en esta temática.  

 

En general, mucha de las acciones públicas en materia de contaminación del aire han estado más orientadas a cumplir con compromisos internacionales que a resolver inquietudes internas (ej. comunicaciones cambio climático) (Murgida, 2014).  Este también parece ser el caso de la Red Federal de Monitoreo Ambiental (https://redfema.ambiente.gob.ar/) donde el Ministerio de Medio Ambiente ha centralizado los datos de monitoreo de agua, suelo y aire según lo acordado en XIX Reunión del Foro de Ministros de Medio Ambiente de América Latina y el Caribe (http://www.pnuma.org/forodeministros/19-mexico/documentos.htm). Sin embargo, el Ministerio solo ha solicitado los datos pero no ha ofrecido a cambio ningún beneficio a quienes los generan más allá de su publicación, ni tampoco ha establecido ningún mecanismo de control de calidad o aseguramiento de la calidad de dichos datos. Solo recientemente, e impulsado por organismos internacionales, se está trabajando con los institutos de normalización (INTI) y los operadores de las estaciones de monitoreo en Argentina, y otros países de Latinoamérica, para evaluar la calidad de los datos y generar capacidades de monitoreo. Esto se da en el marco del Plan de Acción Regional sobre la Contaminación Atmosférica para América Latina y el Caribe (UNEP, 2014). Ante este panorama, son varios los grupos de ciudadanos y científicos en Latinoamerica los que están buscando en los sensores de bajo costo una respuesta ante la falta de datos de calidad del aire. Es interesante destacar que varias de estas iniciativas son de código abierto y han surgido fuera de las instituciones (ie. hackerspaces) con el objetivo de poder accionar los datos junto a afectados en la esfera pública (Cremades, 2013, ACA, ETER)

Referencias

1. ACA, Agentes Calidad del Aire. http://wiki.unloquer.org/personas/brolin/proyectos/agentes_calidad_aire, (Visitada en Junio 2018) 

2. Agilaire, http://agilaire.com/pdfs/ANT.pdf, (Visitada en Junio 2018) 

3. Air quality egg, http://airqualityegg.com/, (Visitada en Junio de 2018) 

4. Aircasting, http://aircasting.org/, (Visitada en Junio de 2018) 

5. Albagli, S., 2015, Open science in question. En Open Science, open issues. Albagli, S., Macie, M.L., Hannud Abdo A. (Eds). IBICT y Unirio. Brasília y Rio de Janeiro. Brasil 292. ISBN 978-85-7013- 111-9. http://livroaberto.ibict.br/handle/1/1061 

6. Arza, V., Fressoli, M. 2016. Proyecto: Ciencia abierta en Argentina: experiencias actuales y propuestas para impulsar procesos de apertura. Informe Final CIECTI. http://stepsamericalatina.org/wp-content/uploads/sites/21/2016/07/Informe-Final-CIECTI.pdf 

7. Clean Air Partnership. 2015. Collaborative Air Quality Monitoring Strategy: Background and Opportunities. Report prepared for: Healthy Public Policy Team, Toronto Public Health. February, 2015. 

8. Clements, A.L.; Griswold, W.G.; RS, A.; Johnston, J.E.; Herting, M.M.; Thorson, J.; Collier-Oxandale, A.; Hannigan, M. Low-Cost Air Quality Monitoring Tools: From Research to Practice (A Workshop Summary). Sensors 2017, 17, 2478. 

9. Castell, N., Dauge, F. R., Schneider, P., Vogt, M., Lerner, U., Fishbain, B., Broday, D., and Bartonova, A., 2017, Can commercial low-cost sensor platforms contribute to air quality monitoring and exposure estimates?, Environ. Int., 99, 293–302, https://doi.org/10.1016/j.envint.2016.12.007. 

10. Carslaw, D.C., Ropkins, K., 2012, Openair d An R package for air quality data analysis, Environmental Modelling & Software 2728. 

11. Clean Air Institute. 2013. La Calidad Del Aire En América Latina: Una Visión Panorámica. www.cleanairinstitute.org/calidaddelaireamericalatina/cai-report-spanish.pdf  

12. Cremades, P., Castro, F., Fernandez, R., Clausen, R., Puliafito, E., (2013), Desarrollo de un monitor abierto de calidad del aire (MACA), VII Encuentro de Investigadores y Docentes de Ingeniería EnIDI, Setiembre 2013, San Rafael, Argentina. EnIDI 2013, Los Reyunos, San Rafael. Mendoza, Argentina  

13. Dye, T., http://tdenviro.com/brief-history-air-quality-sensing/, (Visitada en Junio de 2018) 

14. Esposito, E., De Vito, S., Salvato, M., Bright, V., Jones, R. L., & Popoola, O. (2016). Dynamic neural network architectures for on field stochastic calibration of indicative low cost air quality sensing systems. Sensors and Actuators B: Chemical, 231, 701-713. 

15. ETER, Monitor Abierto de Calidad del Aire, https://etermonitor.net/, (Visitada en Junio 2018) 16. EPA. 2018. Workshop https://www.epa.gov/air-research/workshop-webinar-deliberating-performance-targets-air-quality-sensors 

17. Fenger, J., 2009. Air pollution in the last 50 years – From local to global. Atmospheric Environment, 43(1), pp.13–22. http://linkinghub.elsevier.com/retrieve/pii/S1352231008008960  18. Finlayson-Pitts, B. J., 2010, Atmospheric Chemistry. Proceedings of the National Academy of Sciences of the United States of America, 107(15), 6566–6567. http://doi.org/10.1073/pnas.1003038107

19. Funtowicz, S., Ravetz, J. 2003. Post-normal science. International Society for Ecological Economics (ed.), Online Encyclopedia of Ecological Economics. 

20. Gabrys, J, Pritchard, H, Barratt, B (2016) Just good enough data: figuring data citizenships through air pollution sensing and data stories. Big Data & Society. 3(2): 1-14.

21. Guimarães Pereira, Â., Raes, F., De Sousa Pedrosa, T., Rosa, P., Brodersen, S., Jørgensen, M.S., Ferreira, F., Querol, X., Rea, J., 2009. Atmospheric composition change research: Time to go postnormal? Atmospheric Environment 43, 5423–5432. 

22. Hackair, http://www.hackair.eu/, (Visitada en Junio 2018) 

23. Jacobson, M. 2002. Atmospheric Pollution: History, Science, and Regulation- Cambridge University Press. 412 pp. ISBN-10: 0521010446  

24. Kumar, P., Morawska, L., Martani, C., Biskos, G., Neophytou, M., Di Sabatino, S., Bell, M., Norford, L., Britter, R., 2015. The rise of low-cost sensing for managing air pollution in cities. Environment international 75, 199–205. 

25. Luftdaten, https://luftdaten.info/, (Visitada en Junio 2018) 

26. Mead, M. I., Popoola, O. A. M., Stewart, G. B., Landshoff, P., Calleja, M., Hayes, M., Baldovi, J. J., McLeod, M. W., Hodgson, T. F., Dicks, J., Lewis, A., Cohen, J., Baron, R., Saffell, J. R., and Jones, R. L.: The use of electrochemical sensors for monitoring urban air quality in lowcost, high-density networks, Atmos. Environ., 70, 186–203, https://doi.org/10.1016/j.atmosenv.2012.11.060, 2013. 

27. Merlinsky, G. comp, et al., 2013, Cartografías del conflicto ambiental en Argentina. 1a ed. Buenos Aires: Ciccus, 326 p. ISBN 9789876930338 

28. Murgida, A., Guebel, C., Natenzon, C., Frasco, L., (2013), El aire en la agenda pública: el caso de la ciudad autónoma de Buenos Aires. En Respuestas urbanas al cambio climático en América Latina, CEPAL, 160p, Santiago de Chile, Chile. 

29. Pritchard, H., Gabrys, J., Houston, L., 2018, Re-calibrating DIY: Testing digital participation across dust sensors, fry pans and environmental pollution, New Media & Society, https://doi.org/10.1177/1461444818777473 

30. PTB, 2018, https://www.ptb.de/tc/fileadmin/ResumenTematico_Calidad_del_Aire.pdf (Visitada en Junio 2018) 

31. Puliafito S.E., Allende, D., Panigatti, C. 2013. Contaminación atmosférica e hídrica en Argentina : contribuciones de la IV Reunión Anual PROIMCA y II Reunión Anual PRODECA Puliafito S.E., Allende, D., Panigatti, C (Eds). Buenos Aires : Universidad Tecnológica Nacional. Facultad Regional Mendoza, 2013 

32. PublicLab, https://publiclab.org/, (Visitada en Junio de 2018) 

33. Rai, A.C., Kumar, P., Pilla, F., Skouloudis, A.N., Di Sabatino, S., Ratti, C., Yasar, A., Rickerby, D., 2017. End-user perspective of low-cost sensors for outdoor air pollution monitoring. Science of The Total Environment 607-608, 691-705. 

34. Spinelle, L., Aleixandre, M., Gerboles, M., European Commission, Joint Research Centre, Institute for Environment and Sustainability, 2013. Protocol of evaluation and calibration of low-cost gas sensors for the monitoring of air pollution. Publications Office, Luxembourg.  

35. Spinelle L., Gerboles M., Villani M.G., Aleixandre M., Bonavitacola F., Field calibration of a cluster of low-cost available sensors for air quality monitoring. Part A: ozone and nitrogen dioxide, Sens. And Act. B: Chem., 215, (2015), pp. 249-257. 

36. Smartcitizen, http://www.smartcitizen.me/es/, (Visitada en Junio 2018)  

37. Snyder, E.G., Watkins, T.H., Solomon, P.A., Thoma, E.D., Williams, R.W., Hagler, G.S.W., Shelow, D., Hindin, D.A., Kilaru, V.J., Preuss, P.W., 2013. The Changing Paradigm of Air Pollution Monitoring. Environmental Science & Technology 47, 11369–11377. doi:10.1021/es4022602 

38. UNEP, 2014, http://www.pnuma.org/forodeministros/19mexico/documentos/Borrador%20Plan%20de%20Accion%20Regional%20Contaminacion%20Atmosferica%20Version%20Final%20301013.pdf (Visitada en Junio 2018) 

39. WHO, 2017, Lewis, A.C., Zellweger, C., Schultz, M.G., Tarasova, O.A., (GAW), R.G.S.A.G.,  Technical advice note on lower cost air pollution sensors. World Meteorological Organization and Global Atmosphere Watch. 

40. World Health Organization, 2016, Ambient air pollution: a global assessment of exposure and burden of disease. World Health Organization, http://www.who.int/iris/handle/10665/250141 41. World Bank, 2017, Filling the Gaps: Improving Measurement of Air Quality in Developing Countries Workshop, http://www.worldbank.org/en/events/2017/07/25/filling-the-gaps-improving-measurement-of-air-quality-in-developing-countriesworkshop#2 

42. Zimmerman, N., Presto, A.A., Kumar, S.P.N., Gu, J., Hauryliuk, A., Robinson, E.S., Robinson, A.L., Subramanian, R., 2018. A machine learning calibration model using random forests to improve sensor performance for lower-cost air quality monitoring. Atmos. Meas. Tech. 11, 291-313