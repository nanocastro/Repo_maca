**LUFTDATEN**

SDS011/DHT22

[http://luftdaten.info/feinstaubsensor-bauen/ 1](http://luftdaten.info/feinstaubsensor-bauen/)

**Upload Firmware**

%USERPROFILE%\AppData\Local\Arduino15\packages\esp8266\tools\esptool\0.4.13\esptool.exe -vv -cd nodemcu -cb 57600 -ca 0x00000 -cp COM5 -cf C:\Users\nano\Downloads\latest_en.bin

Cambiar el puerto COM y la carpeta local donde está alojado el .bin

**Configuración**

La primera vez se configura la red wi-fi y se especifica cuales son los sensores que estás usando. 

Anotar el Id del NodeMCU (no es el mismo Id del sensor en el Mapa, una vez que se suscribe el sensor lo ponen en el mapa y ahi te asigna un Sensor-Id que es el que usas para recuperar los datos)

Para acceder a luego a la configuración 

[http://feinstaubsensor-{nodemcu-id}.local](http://feinstaubsensor-{nodemcu-id}.local)

JSON con los datos de un sensor cada 5 minutos

[http://api.luftdaten.info/v1/sensor/sensorid /](http://api.luftdaten.info/v1/sensor/sensorid)

Acá podes consultar todo lo relacionado a las API's

[https://github.com/opendata-stuttgart/meta/wiki/APIs](https://github.com/opendata-stuttgart/meta/wiki/APIs)

Archivo de datos

[http://archive.luftdaten.info/](http://archive.luftdaten.info/)

Firmware** **

[https://github.com/opendata-stuttgart/sensors-software/tree/master/airrohr-firmware](https://github.com/opendata-stuttgart/sensors-software/tree/master/airrohr-firmware)

La conexión al NodeMCU para los distintos sensores (plantower, bme) se puede consultar en el readme.

[https://github.com/opendata-stuttgart/sensors-software/blob/master/airrohr-firmware/Readme.md](https://github.com/opendata-stuttgart/sensors-software/blob/master/airrohr-firmware/Readme.md)

Jupyterlab notebook para sacar datos de Luftdaten

[https://github.com/dr-1/airqdata](https://github.com/dr-1/airqdata)

**MACA-Luftdaten**

En  Argentina estamos a 5 horas de la hora alemana y a -3 UTC

**LuftdatenMACA1 SDS011 - DHT22**

ChipID 16614917 

Sensor ID: 16013 y 16014 desde el 9/8/2018

MAC: 5C:CF:7F:FD:86:05

Firmware version: NRZ-2017-099

[https://www.madavi.de/sensor/graph.php?sensor=esp8266-16614917-dht](https://www.madavi.de/sensor/graph.php?sensor=esp8266-16614917-dht)

[https://www.madavi.de/sensor/graph.php?sensor=esp8266-16614917-sds011](https://www.madavi.de/sensor/graph.php?sensor=esp8266-16614917-sds011)

**LuftdatenETER1 - PLANTOWER 5003/DHT22**

[https://www.madavi.de/sensor/graph.php?sensor=esp8266-3912589-pms](https://www.madavi.de/sensor/graph.php?sensor=esp8266-3912589-pms)

ChipID: 3912589

**LuftdatenMACA2 - PLANTOWER 7003/DHT22**

[https://www.madavi.de/sensor/graph.php?sensor=esp8266-4250303-dht](https://www.madavi.de/sensor/graph.php?sensor=esp8266-4250303-dht)

[https://www.madavi.de/sensor/graph.php?sensor=esp8266-4250303-dht](https://www.madavi.de/sensor/graph.php?sensor=esp8266-4250303-pms)

ChipID: 4250303

Sensor ID: 16632 (PMS) y 16632 (DHT) desde el 10/10/2018

**Campaña fines setiembre**



*   Conectamos el 26/9 y a la noche llovió. Se ve en los gráficos
*   27/9 nublado y leve lluvia
*   [https://losandes.com.ar/article/view?slug=en-mendoza-hubo-3-tipos-de-viento-en-menos-de-una-semana](https://losandes.com.ar/article/view?slug=en-mendoza-hubo-3-tipos-de-viento-en-menos-de-una-semana)
*   Llovio el 27/10 a la noche

Eventos agosto:



*   feriado del lunes 20. 
*   Picos del 23. Viento fuerte en lunlunta desde las 20. 22 lluvia.
*   Lunes 3/9 cierre caminos universidad

**En Grafana**

[https://luftdaten.getkotori.org/grafana/d/000000009/feinstaub-verlauf?refresh=5m&orgId=1&var-Location=Mendoza,%20Mza.,%20AR&var-Location=Unc,%20Ciudad%20de%20Mendoza,%20Mza.,%20AR](https://luftdaten.getkotori.org/grafana/d/000000009/feinstaub-verlauf?refresh=5m&orgId=1&var-Location=Mendoza,%20Mza.,%20AR&var-Location=Unc,%20Ciudad%20de%20Mendoza,%20Mza.,%20AR)

Otros link

[http://www.opengeiger.de/Feinstaub/feinstaub.html](http://www.opengeiger.de/Feinstaub/feinstaub.html)