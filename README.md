# MACA - Monitor Abierto de Calidad de Aire  
**Laboratorio de Análisis Instrumental de la DETI Facultad de Ingeniería - Universidad Nacional de Cuyo  
LabFD - UTN-FRM**  [(Web)](www.frm.utn.edu.ar/labfd)

Durante este proyecto se construyeron y evaluaron varios prototipos de monitores de calidad del aire.  
Aquí se puede encontrar la documentación relativa a dichos procesos.
> **Este proyecto tiene su continuación ahora en [Monitoreo Abierto de la Calidad del Aire](https://gitlab.com/nanocastro/monitoreoabierto/)**  

![luftdaten](/Imagenes/Gases/Luftdaten/20180808_115125.jpg)  

# Prototipos de MACA

## Prototipo 1 - Ozono  
El primer prototipo fue realizado en el año 2013.  
### Documentación
La documentación y la evaluación de este prototipo se puede encontrar en monitorabierto.wikidot.com/desarrollo:prototipo1

## Prototipo 2 - Gases y particulado
Este dispositivo se termino de construir y evaluar a principios de 2018.
### Documentación
Una descripción de este segundo dispositivo se puede encontrar en http://monitorabierto.wikidot.com/desarrollo:prototipo2
Este prototipo se separó luego en dos: uno para gases y otro para particulado. 
Aqui se puede consultar la documentación de dichos dispositivos
[Hardware](https://gitlab.com/nanocastro/Repo_maca/tree/master/Hardware)  
[Firmware](https://gitlab.com/nanocastro/Repo_maca/tree/master/Firmware)  
### Field testing
Durante Enero del 2018 realizamos una campaña de medición con este prototipo de MACA en paralelo a una estación con equipos de referencia.  
Los [resultados](http://rpubs.com/dmonge/calibracion_maca) obtenidos no fueron buenos. Los sensores de gases presentan una baja correlación con la referencia 
y los datos del sensor de particulado (Shinyei PPD42) no parecen atendibles. Es necesario, además, trabajar con regresiones multivariables o algoritmos de 
aprendizaje de máquinas que permitan minimizar/eliminar interferencias de temperatura, humedad e interferencias cruzadas de otros gases 
[(Ver Referencias)](https://gitlab.com/nanocastro/Repo_maca/tree/master/Referencias/Gases%20-%20ML).  
[Aquí](https://gitlab.com/nanocastro/Repo_maca/tree/master/Datos/Para%20analizar) se pueden consultar los datos de esta campaña. Leer calibracion.md para referencia.  

## Prototipo 3 - Particulado
Se ensamblaron dos  dos réplicas Luftdaten que utilizan dos sensores distintos para PM, el Plantower 7003 y el NOVA SDS011.  
### Documentación
[Aquí](https://gitlab.com/nanocastro/Repo_maca/blob/master/Luftdaten.md) se pueden consultar algunas cuestiones relacionadas al ensamblado, a la identificación de los sensores y el acceso a los datos.  
Réplica/adaptación del prototipo de monitoreo de la plataforma https://luftdaten.info/.   
En el [mapa de la plataforma](https://deutschland.maps.luftdaten.info/#6/-32.938/-68.801) se pueden encontrar ambos sensores identificados como 16013 y 16632
Ambos sensores se hayan co-localizados y sus datos se pueden se pueden consultar on-line en  
[PM10 y PM2.5 de sensor SDS011](https://www.madavi.de/sensor/graph.php?sensor=esp8266-16614917-sds011)  
[PM10 y PM2.5 de sensor Plantower7003](https://www.madavi.de/sensor/graph.php?sensor=esp8266-4250303-pms)    
[Temp y HR](https://www.madavi.de/sensor/graph.php?sensor=esp8266-16614917-dht)    
### Field testing
Este es un [análisis de los datos](http://api.rpubs.com/nanocastro/493740) con la librería OpenAir de aproximadamente 6 meses de mediciones con ambos dispositivos.
También estamos realizando una [calibración con un equipo de referencia](http://api.rpubs.com/nanocastro/498766) aunque no tenemos certeza aún de los resultados.  
La conclusión inicial es que es necesario revisar la calibración del equipo de referencia.

# Conclusiones
Como conclusión de casi 4 años de trabajo en el desarrollo de prototipos locales para el monitoreo de la calidad del aire hemos decidido no “reinventar la rueda” y sumarnos a las comunidades de desarrollo existentes trabajando en la réplica y adaptación de sus prototipos.  
Esta decisión se debe en gran medida a dificultades tales como la falta de soporte para debugging/calibración de los prototipos y los largos tiempos de desarrollo, pero también a la proliferación y los avances conseguidos por comunidades globales dedicadas al monitoreo de la contaminación del aire tales como PurpleAir, Hackair y Luftdaten.  
Los datos de los sensores son publicados en tiempo real y son abiertos, es decir, que se pueden descargar de la misma plataforma.  
Por otro lado, la experiencia adquirida durante el proyecto nos ha permitido avanzar en una mejor conceptualización socio-técnica de la contaminación del aire, entendiendo que no solo se trata de materia flotando a nuestro alrededor sino que es también el resultado de un ensamblaje de personas, instituciones, asociaciones, colectivos, máquinas, infraestructuras, sensores, estaciones de medición, servidores, laboratorios  que lo producen, miden, regulan, cuidan, conservan, etc
Desde este nuevo paradigma socio-tecnico dejamos de pensar entonces en los dispositivos como elementos meramente técnicos sino también desde sus posibilidades relacionales (socio-políticas). Por este motivo, consideramos fundamental la necesidad de interactuar con los distintos actores afectados e involucrados en el estudio y la gestión de la calidad del aire. Para ello, y siguiendo los aprendizajes de las comunidades de desarrollo de hardware científico abierto, elegimos como vehículo/infraestructura las herramientas de monitoreo de partículas abiertas (Luftdaten y Hackair) y los talleres prácticos como forma de comenzar a tejer nuevas relaciones y desarrollar nuevas prácticas entorno al monitoreo de la calidad del aire a nivel local.  
Consideramos que la integración ingeniosa entre actores interesados y herramientas abiertas de monitoreo, y su sintonización con las configuraciones materiales, espaciales y temporales locales,  nos permitirá obtener datos lo “suficientemente buenos” para dar cuenta de las distintas historias que pueden ser contadas acerca del material particulado que respiramos.

> *[Aqui](https://gitlab.com/nanocastro/monitoreoabierto/) se puede consultar la continuidad de este proyecto*

## Otros productos del proyecto
* Mapa conceptual de la calidad del aire
Por otro lado, hemos avanzado en la conceptualización de la problemática de la calidad del aire mapeando a todos los actores involucrados y estableciendo un marco cognitivo general con fines educativos y a los fines de mejor diseñar la forma de participación de los ciudadanos en el monitoreo de la calidad del aire.  
http://monitorabierto.wikidot.com/desarrollo:mapaconceptual 
* Review de sensores low-cost
En el marco de este proyecto hemos realizado una pequeña [revisión](https://gitlab.com/nanocastro/Repo_maca/blob/master/Review%20low-cost%20AQ.md) del estado del arte de los sensores low-cost a nivel global y local.  
* Recursos 
También hemos trabajado, junto a la comunidad GOSH en una [compilación](https://gitlab.com/nanocastro/Repo_maca/blob/master/AQ-Resources.md) de recursos de monitoreo de calidad del aire con hardware abierto y sensores de bajo costo. 

## Publicaciones
* Participamos del Primer [Workshop](https://www.cientopolis.org/workshop/) Argentino de ciencia abierta y ciudadana con este [Poster](http://monitorabierto.wdfiles.com/local--files/desarrollo%3Arecursos/MACA%20poster.pdf)
* Participamos de la [Capacitación](http://www.uncuyo.edu.ar/desarrollo/capacitacion-en-calidad-de-aire-su-legislacion-y-gestion-en-la-provincia-de-mendoza) en Calidad de Aire, su legislación y gestión en la provincia de Mendoza organizada por el ICA de la UN Cuyo. Aquí nuestra [http://monitorabierto.wikidot.com/local--files/desarrollo:recursos/Presentacion_Legislatura.pdf presentación].
* A mediados de abril 2017 estuvimos participando de [GOSH](http://openhardware.science/logistics/) con nuestro segundo prototipo de MACA. Conocimos cara a cara a las personas que vienen inspirando y alimentando nuestro trabajo desde hace ya un par de años. Si les interesa pueden seguir la actividad de la comunidad GOSH en este [https://forum.openhardware.science/ foro].
* Estamos en el mapa de ciencia abierta Argentina elaborado por STEPS América Latina [Mapa](https://stepsamericalatina.org/ciencia-abierta-el-mapa-argentino/) 
* Presentamos el Proyecto "Herramientas tecnológicas abiertas para la generación colaborativa de conocimiento sobre la calidad del aire local." en la Convocatoria a proyectos bianuales Tipo I de la Sectyp - UN Cuyo. Aquí se puede consultar el plan de trabajo [Plan](http://monitorabierto.wdfiles.com/local--files/desarrollo%3Arecursos/plandetrabajo%20final.pdf).
* Poster presentado en las XXIV Jornadas de Investigación y VI Jornadas de Posgrado de la Universidad Nacional de Cuyo, UN Cuyo, Diciembre de 2015. 
* Castro, F., Cremades, P., Fernandez, R., Clausen, R., Puliafito, E., (2013), Desarrollo de un monitor abierto de calidad del aire (MACA), VII Encuentro de Investigadores y Docentes de Ingeniería EnIDI, Setiembre 2013, San Rafael, Argentina. EnIDI 2013, Los Reyunos, San Rafael. Mendoza, Argentina [[file Enidi - Maca.pdf | Archivo]]
* Presentación al Cuarto Congreso del Proyecto Integrador para la Mitigación de la Contaminación Atmosférica (PROIMCA), UTN - Facultad Regional Córdoba, Junio 2013.   
* Poster presentado en las XXIII Jornadas de Investigación y V Jornadas de Posgrado de la Universidad Nacional de Cuyo, UN Cuyo, Abril de 2013. 

## Información general del proyecto (old)
Wiki del proyecto [MACA](http://www.monitorabierto.wikidot.com)


<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.