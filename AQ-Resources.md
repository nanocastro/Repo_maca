<h1 id="heading--aqtop">Air Quality - Resources on open source low-cost monitoring</h1>

This is a place to gather info/links/projects about air quality.
This was a proposed action at GOSH 2018

**Table of contents**  
<a href="#heading--introduction">**Introduction**</a>  
<a href="#heading--biginstitutions">**What do big institutions say about air quality and low-cost monitoring?**</a>  
<a href="#heading--reviews">**Scientific reviews of low-cost air quality monitoring**</a>  
<a href="#heading--performance">**Evaluation of capabilities and performance of low-cost instruments and sensors**</a>  
<a href="#heading--sensorcomments">Some comments about low-cost sensors</a>  
<a href="#heading--sensorcomercial">Comercial sensors</a>  
<a href="#heading--opensource">**Existing open source sensors/platforms**</a>  
<a href="#heading--datastandards">**Data standards**</a>  
<a href="#heading--fieldprotocols">**Field testing calibration protocols**</a>  
<a href="#heading--aqart">**AQ visualization/Art**</a>  
<a href="#heading--aqgosh">**GOSH members AQ projects**</a>  
<a href="#heading--aqcitizen">**Citizen science and political ecology readings**</a>  

<h2 id="heading--introduction">Introduction</h2>

Here you can find a [brief summary and background](https://docs.google.com/document/d/16UxlfovQklSgVwUZOlU4NB9FK141gPLnzSm11Bt30C8/edit?usp=sharing ) about stat of the art in AQ monitoring 

Here a [cognitive map of AQ](http://monitorabierto.wikidot.com/desarrollo:mapaconceptual ) 

(just in spanish for now sorry)


<h2 id="heading--biginstitutions">What do big institutions say about air quality and low-cost monitoring?</h2>

World Health Organization. Outdoor air quality. Statistics and reports
[http://www.who.int/phe/health_topics/outdoorair/en/](http://www.who.int/phe/health_topics/outdoorair/en/)

World Bank Workshop. 2017. Filling the gap. Improving Measurement of Air Quality in Developing Countries 
[http://www.worldbank.org/en/events/2017/07/25/filling-the-gaps-improving-measurement-of-air-quality-in-developing-countries-workshop#2](http://www.worldbank.org/en/events/2017/07/25/filling-the-gaps-improving-measurement-of-air-quality-in-developing-countries-workshop#2). 

World Meteorological Organization. Report on low cost sensors.
[http://www.ccacoalition.org/en/resources/low-cost-sensors-measurement-atmospheric-composition-overview-topic-and-future](http://www.ccacoalition.org/en/resources/low-cost-sensors-measurement-atmospheric-composition-overview-topic-and-future)

EPA Air Sensor Toolbox. General guidelines for citizens to choose and use aq sensors 
[https://www.epa.gov/air-sensor-toolbox](https://www.epa.gov/air-sensor-toolbox)

<h2 id="heading--reviews">Scientific reviews of low-cost air quality monitoring</h2>

*   Clements, A.L.; Griswold, W.G.; RS, A.; Johnston, J.E.; Herting, M.M.; Thorson, J.; Collier-Oxandale, A.; Hannigan, M. Low-Cost Air Quality Monitoring Tools: From Research to Practice (A Workshop Summary). Sensors 2017, 17, 2478. 
*   Rai, A.C., Kumar, P., Pilla, F., Skouloudis, A.N., Di Sabatino, S., Ratti, C., Yasar, A., Rickerby, D., 2017. End-user perspective of low-cost sensors for outdoor air pollution monitoring. Science of The Total Environment 607-608, 691-705

<h2 id="heading--performance">Evaluation of capabilities and performance of low-cost instruments and sensors</h2>

Air Quality Sensor Performance Evaluation Center. Maybe the most complete site of aq sensors performance evaluation. They do field and lab evaluations with reference instruments
[http://www.aqmd.gov/aq-spec ](http://www.aqmd.gov/aq-spec)

EPA Performance Targets for Air Quality Sensors Workshop (2018). Recent workshop discussing the performance of low-cost sensors for PM and ozone
[https://www.epa.gov/air-research/presentations-deliberating-performance-targets-air-quality-sensors-workshop](https://www.epa.gov/air-research/presentations-deliberating-performance-targets-air-quality-sensors-workshop)

Overview of Air Quality sensors experiments. Chinese site with lots of info on sensors and also aq data. 
[http://aqicn.org/sensor/](http://aqicn.org/sensor/)

Old site with info on manufacturers and evaluations reports
[www.snuffle.org](http://www.snuffle.org/)

<h3 id="heading--sensorcomments">Some comments about low-cost sensors</h3>

Particulate Matter 
PM is the most measured component in outdoors air quality mainly because of its harmful potential and because low-cost PM sensors are much more accurate than gas sensors at the moment.

Gas sensors 
There are mainly of two kinds, metal oxide sensors and electrochemical sensors. The first ones are cheaper but they have still lots of issues (ie T and RH interference). Electrochemical sensors are the ones improving faster and have been used in several instruments. They are not so easy to implement and there are some issues of calibration also. 

<h3 id="heading--sensorcomercial">Comercial sensors</h3>

*   [MICS](https://sgx.cdistore.com/products/sgx-sensortech/metaloxide-gas-sensor). Gases
*   [SPEC](https://www.spec-sensors.com/product-category/digital-gas-sensors-iot/). Gases
*   [Alphasense](http://www.alphasense.com/index.php/safety/products/). Gases and PM
*   [Figaro](http://www.figarosensor.com/). Gas
*   [Citytech](https://www.citytech.com/). Gas
*   [Plantower](http://www.plantower.com/en/). PM
*   [Nova](http://www.inovafitness.com/en/a/index.html). PM

<h2 id="heading--opensource">Existing open source sensors/platforms</h2>

### **Luftdaten.info**
[Luftdaten.info](https://luftdaten.info/) 
Maybe the most popular open source PM sensor. The standard setup uses the Nova SDS011 but you can use also any Plantower. Easy to build. Big community of users. Data is public. 
Some extra details about luftdaten:
*   Config. Wi-fi and sensor setup can be configured on the first use. You should write the Id of the NodeMCU (is not the same Id of the sensor in the map). To visualize the data of the sensor in the map you must subscribe it to luftdaten and then you get your Sensor-Id which you can use to recover the data. 
*   To access the sensor configuration. [http://feinstaubsensor-{nodemcu-id}.local](http://feinstaubsensor-{nodemcu-id}.local)
*   JSON with data of the sensors everty 5 min [http://api.luftdaten.info/v1/sensor/sensorid /](http://api.luftdaten.info/v1/sensor/sensorid)
*   Everything related to API [https://github.com/opendata-stuttgart/meta/wiki/APIs](https://github.com/opendata-stuttgart/meta/wiki/APIs)
*   Data archive [http://archive.luftdaten.info/](http://archive.luftdaten.info/) 

### **hackAIR**
[https://www.hackair.eu/](https://www.hackair.eu/)
Open technology platform that you can use to access, collect and improve air quality information in Europe. They are only measuring PM for the moment
You can follow a tutorial to build a home sensor. Uses the Nova SDS011 PM sensor. Data is public. There are also two other methods to check for PM, an artisanal method and a visual method.

### **Openaq** 
[https://openaq.org/#/?_k=smqdur](https://openaq.org/#/?_k=smqdur)
Aggregate physical air quality data from public data sources provided by government, research-grade and other sources. Mainly official sources of data. 

### **Aircasting** 
[http://aircasting.org/](http://aircasting.org/)
AirCasting is an open-source, end-to-end solution for collecting, displaying, and sharing health and environmental data using your smartphone.The current version of the monitor is Airbeam 2. It uses a Plantower 7003 to measure PM. There are some older versions of the Airbeam that can sense gases 

### **Airquality Egg** 
[https://airqualityegg.com/home](https://airqualityegg.com/home)
This was maybe the first known open source AQ monitor. Previous versions were used to monitor gases but they had a lot of issues with data quality. The last version uses Plantower 5003 to measure PM. 

<h2 id="heading--datastandards">Data standards</h2>

Some already existent data standards:
*   This is a data standard for timestamp
[https://www.edf.org/health/data-standards-date-and-timestamp-guidelines](https://www.edf.org/health/data-standards-date-and-timestamp-guidelines)
*   This is the data standard for Openaq platform
[https://github.com/openaq/openaq-data-format](https://github.com/openaq/openaq-data-format)

<h2 id="heading--fielprotocols">Field testing calibration protocols </h2>

From the European Union [European Union](http://www.aqmd.gov/docs/default-source/aq-spec/resources-page/european-commission-jrc---sensor-evaluation-report.pdf) (Responsible - M. Gerboles) 

From US [AQ-SPEC](http://www.aqmd.gov/docs/default-source/aq-spec/protocols/sensors-field-testing-protocol.pdf?sfvrsn=0) (Responsible - A. Polidori) 

<h2 id="heading--aqart">AQ visualization/Art </h2>

*   Nerea Calvillo. [https://www.youtube.com/watch?v=I7Ff29FxeFM](https://www.youtube.com/watch?v=I7Ff29FxeFM). She has many other cool projects. 
*   Andrea Polli. [http://eco-publicart.org/particle-falls/](http://eco-publicart.org/particle-falls/)
*   Nut Brother.  [http://www.citylab.com/weather/2015/12/beijings-smog-is-so-bad-you-can-turn-it-into-a-brick/418341/](http://www.citylab.com/weather/2015/12/beijings-smog-is-so-bad-you-can-turn-it-into-a-brick/418341/)
*   Carbon Arts. [http://www.carbonarts.org/things-we-like/air-pollution/](http://www.carbonarts.org/things-we-like/air-pollution/)
*   Studio Roosegaarde [https://www.studioroosegaarde.net/uploads/files/2015/07/24/225/Smog%20Free%20Project%20datasheet%20Roosegaarde.pdf](https://www.studioroosegaarde.net/uploads/files/2015/07/24/225/Smog%20Free%20Project%20datasheet%20Roosegaarde.pdf)
*   Helen Evans. [http://eco-publicart.org/nuage-vert-green-cloud/](http://eco-publicart.org/nuage-vert-green-cloud/)

<h2 id="heading--aqgosh">GOSH members AQ projects</h2>

**Juli - ETER - Buenos Aires** @jarancio 
Open source Air Quality Monitor for community science. 
[https://github.com/rlyehlab/eter](https://github.com/rlyehlab/eter)
It is based on a Plantower 5003 and NodeMCU. 

**Nano - MACA -  Mendoza** @nanocastro 
Monitor abierto de calidad del aire 
[www.monitorabierto.wikidot.com](http://www.monitorabierto.wikidot.com) 
[https://gitlab.com/nanocastro/Repo_maca](https://gitlab.com/nanocastro/Repo_maca) 
[https://www.madavi.de/sensor/graph.php?sensor=esp8266-16614917-sds011](https://www.madavi.de/sensor/graph.php?sensor=esp8266-16614917-sds011)
[https://www.madavi.de/sensor/graph.php?sensor=esp8266-4250303-pms](https://www.madavi.de/sensor/graph.php?sensor=esp8266-4250303-pms)

**Proyect from Andriy - Ukraine** @Andriy 
Based on a MiCS-6814 for gases and a Plantower 3003
[https://github.com/AndriyHz/Teensy-3.2-and-Thingspeak](https://github.com/AndriyHz/Teensy-3.2-and-Thingspeak)
[https://thingspeak.com/channels/371428](https://thingspeak.com/channels/371428)

**Rachel Hu - Luftdaten - Shenzhen / Hong Kong** @RachelH 
Luftdaten workshops in SZ and HK
[http://www.huodongxing.com/event/9451533598000](http://www.huodongxing.com/event/9451533598000)

**Hamilton - Semáforos de polución- Colombia** @librepensante.org 
[http://librepensante.org/](http://librepensante.org/)

JP? @juanpedro.maestre 

Emmanuelle?? @EmmanuelK @Emmanuel

VJpixel ?? @pixel 

### **Other open source AQ monitoring projects** 

Waag - PM+NO2 
[https://waag.org/en/project/urban-airq](https://waag.org/en/project/urban-airq)
[https://www.atmos-meas-tech.net/11/1297/2018/amt-11-1297-2018.pdf](https://www.atmos-meas-tech.net/11/1297/2018/amt-11-1297-2018.pdf)

Medellin - PM - Plantower 
[http://wiki.unloquer.org/personas/brolin/proyectos/agentes_calidad_aire](http://wiki.unloquer.org/personas/brolin/proyectos/agentes_calidad_aire)

Ourairquality - PM - Plantower5003
[https://ourairquality.org/ 1](https://ourairquality.org/)

Less-Smog.org - ([on Kitspace](https://kitspace.org/boards/github.com/less-smog/hardware/)) - Plantower PMS3003/5003/7003 + HTU21D (Humidity). 

<h2 id="heading--aqcitizen">Citizen science and political ecology readings</h2>

*   Gabrys, J, Pritchard, H, Barratt, B (2016) Just good enough data: figuring data citizenships through air pollution sensing and data stories. Big Data & Society. 3(2): 1-14.
*   Pritchard, H., Gabrys, J., Houston, L., 2018, Re-calibrating DIY: Testing digital participation across dust sensors, fry pans and environmental pollution, New Media & Society, https://doi.org/10.1177/1461444818777473 
*   Nerea Calvillo, 2018, Reclamar el aire, La aventura de aprender, [http://laaventuradeaprender.educalab.es/documents/10184/73911/NereaCalvillo_aireD.pdf](http://laaventuradeaprender.educalab.es/documents/10184/73911/NereaCalvillo_aireD.pdf)
*   Calvillo, N. (2014). Sensing Aeropolis. Urban air monitoring devices in Madrid, 2006-2010. Tesis (Doctoral)
*   S.Graham, 'Life Support: the Political Ecology of Urban Air', City, 19(2–3), 2015, pp. 192–215.