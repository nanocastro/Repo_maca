---
title: "Medición de PM en cuarentena COVID 19"
author: "Nano Castro"
date: "7 de julio de 2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
## Datos de la campaña
Duración: desde el 6 de abril al 15 de mayo 2020
Relojes: a las 17:58 de MACA-Luftdaten son las 17:50 en Thermo

Nomeclatura sensores:
ID nodo   Color     ID sensor   Tipo    Numero
16614917            16013       sds011  1
16614917            16014       dht22   2
3912589   Naranja   34021       pms7003 3
3912589   Naranja   34022       dht22   4
413996    Lila      34427       pms7003 5
413996    Lila      34428       dht22   6
7233236   Azul      34429       sds018  7
7233236   Azul      34430       bme280  8


## Lectura de los datos de los datos de los sensores MACA-Luftdaten

Para hacer la lectura se definene los datos del sensor y las fechas de inicio y fin de la busqueda


```{r lectura, echo=FALSE}
library(lubridate)
library(tidyr)
library(tidyverse)
library(anytime)
library(openair)
library(ggplot2)
sensor=c("16013","16014","34021","34022","34427","34428","34429","34430")
sensor_type=c("sds011","dht22","pms7003","dht22","pms7003","dht22","sds011","bme280")

for (i in 1:length(sensor)){
#Lee datos y cambia formato date
luft_data<-read.csv(paste("luft_",sensor[i],"calib5.csv",sep=""))
# luft_data_1$date<-as.POSIXct(luft_data_1$date)
# anytime asUTC convierte un timestamp en UTC a la hora local...??
luft_data$date<- anytime(as.factor(luft_data$date),asUTC=TRUE)
luft_data$date<- luft_data$date-8*60
# str(luft_data)
#Calcula promedios 1 hora
luft_data_h<-timeAverage(luft_data,avg.time="30 min")
assign(paste(sensor_type[i],"_",sensor[i],sep=""),luft_data_h)
# luft_data_1$date<-with_tz(luft_data_1$date,tzone="America/Argentina/Mendoza")
}
#str(luft_data_1_h)
#summary(luft_data_1_h)
#summary(luft_data_2_h)

```

## Datos crudos del equipo de referencia

```{r lecturaref, echo=FALSE}
## Armado de un solo data frame con datos de los sensores y de referencia
ref_data<-read.table(paste("INV_cuarentena.txt",sep=""))
colnames(ref_data)<-c("dia","hora","code","num","PM")
ref_data$dia<-as.Date(ref_data$dia, format="%y-%m-%d")
ref_data<-unite(ref_data, "date", c("dia","hora"),sep="-")
ref_data$date<- anytime(as.factor(ref_data$date))
#ref_data$date<- ref_data$date + minutes(7)
str(ref_data)
# timePlot(ref_data, pollutant = c("PM"),
#          y.relation = "free",group=TRUE,statistic="percentile",avg.time="30 min", percentile=95)

```

```{r join, echo=FALSE}

#eval(parse(text=(paste(sensor_type[1],"_",sensor[1],sep=""))))
#Join de particulado
luft_join<-inner_join(subset(sds011_16013, select= c(date,PM10,PM2.5)),subset(pms7003_34021, select=c(date,PM10,PM2.5) ),by="date",type="inner")
luft_join<-inner_join(luft_join,subset(pms7003_34427, select=c(date,PM10,PM2.5) ),by="date",type="inner")
luft_join<-inner_join(luft_join,subset(sds011_34429, select=c(date,PM10,PM2.5) ),by="date",type="inner")
#Join con datos meteo BME 280
luft_join<-inner_join(luft_join,subset(bme280_34430, select=c(date,temperature,humidity) ),by="date",type="inner")

# Solo se midio PM10 en este periodo
cal_data<-inner_join(ref_data,luft_join,by="date",type="inner")
# Limito los datos a 1 mes de campaña
cal_data<-subset(cal_data,date < as.Date("2020-05-07 12:00:00")  & PM > 0)

```
## Grafico de calendario para medias de 24 horas

```{r calendar, echo=FALSE}

calendarPlot(cal_data, pollutant = "PM", annotate = "value", lim =50)

```

## Gráficos de correlación entre sensore de referencia y sensores low-cost para PM10

```{r pairs, echo=FALSE}
library(ggplot2)
library(GGally)

ggpairs(cal_data, columns = c("PM","PM10.x","PM10.y","PM10.x.x","PM10.y.y"), columnLabels = c("Ref","16013_sds011","34021_PMS","34427_PMS","34429_sds018"))


```

```{r timeplot1, echo=FALSE}
#png("todosagosto.png", width = 3.5 * 300, height = 3 * 300, res = 300) 
timePlot(cal_data, pollutant = c("PM","PM10.x","PM10.y","PM10.x.x","PM10.y.y"), y.relation = "free", smooth=FALSE, ci=95,name.pol=c("Ref","16013_sds011","34021_PMS","34427_PMS","34429_sds018"), ylab = "pm10 (ug/m3)")

```

## Grafico con promedios de 24 horas

```{r timeplot2, echo=FALSE}
timePlot(cal_data, pollutant = c("PM","PM10.x","PM10.y","PM10.x.x","PM10.y.y"), y.relation = "free", smooth=FALSE, ci=95,name.pol=c("Ref","16013_sds011","34021_PMS","34427_PMS","34429_sds018"),group=TRUE,avg.time="day",ylab = "pm10 (ug/m3)")

```

## Grafico de la variación diaria y semanal PM10
Concentraciones sin normalizar

```{r variaciondia1, echo=FALSE}
timeVariation(cal_data, pollutant = c("PM","PM10.x","PM10.y","PM10.x.x","PM10.y.y"),normalise="FALSE", name.pol=c("Ref","16013_sds011","34021_PMS","34427_PMS","34429_sds018"),
              ylab = "PM10 (ug/m3)") 
```

Concentraciones normalizadas

```{r variaciondia2, echo=FALSE}
timeVariation(cal_data, pollutant = c("PM","PM10.x","PM10.y","PM10.x.x","PM10.y.y"),normalise="TRUE", name.pol=c("Ref","16013_sds011","34021_PMS","34427_PMS","34429_sds018"),
              ylab = "PM10 (ug/m3)")  

```

## Influencia de la humedad en las mediciones de PM10

```{r timeplotweath1, echo=FALSE}
timePlot(cal_data, pollutant = c("PM","PM10.x","PM10.y","PM10.x.x","PM10.y.y","temperature","humidity"), y.relation = "free", smooth=FALSE, ci=95,name.pol=c("Ref","SDS011","PMS7003-A","PMS7003-B","SDS011","Temp(°C)","HR"),date.breaks=4,fontsize=8,ylab="PM10 (ug/m3)",xlab="Series de tiempo - PM10", key="FALSE")
```

```{r HRanalisis, echo=FALSE}
scatterPlot(cal_data, x = "PM", y = "PM10.x", smooth = FALSE,linear = TRUE , main="Correlación PM10",xlab ="PM10(ug/m3) - Ref", ylab="PM10(ug/m3) - SDS011")

```

```{r HRanalisis1, echo=FALSE}
scatterPlot(cal_data, x = "PM", y = "PM10.y", smooth = FALSE,linear = TRUE, main="Correlación PM10 para distinta humedad",xlab ="PM10(ug/m3) - Ref",
            ylab="PM10(ug/m3) - PMS7003-A")

```
