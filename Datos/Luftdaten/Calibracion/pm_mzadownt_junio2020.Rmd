---
title: "Monitoreo material particulado en la ciudad"
author: "Nano Castro"
date: "10 de julio de 2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Datos de la campaña


Nomeclatura sensores:
ID nodo   Color     ID sensor   Tipo    Numero
16614917            16013       sds011  1
16614917            16014       dht22   2
3912589   Naranja   34021       pms7003 3
3912589   Naranja   34022       dht22   4
413996    Lila      34427       pms7003 5
413996    Lila      34428       dht22   6
7233236   Azul      34429       sds018  7
7233236   Azul      34430       bme280  8


## Lectura de los datos de los datos de los sensores MACA-Luftdaten

Para hacer la lectura se definene los datos del sensor y las fechas de inicio y fin de la busqueda


```{r lectura, echo=FALSE}
library(lubridate)
library(tidyr)
library(tidyverse)
library(anytime)
library(openair)
library(ggplot2)

rm(list=ls())

sensor=c("34021","34022","34427","34428","34429","34430")
sensor_type=c("pms7003","dht22","pms7003","dht22","sds011","bme280")
nodos<-data.frame(sensor,sensor_type)

for (i in 1:length(sensor)){
#Lee datos y cambia formato date
luft_data<-read.csv(paste("luft_",sensor[i],"mzadownt.csv",sep=""))
# luft_data_1$date<-as.POSIXct(luft_data_1$date)
# anytime asUTC convierte un timestamp en UTC a la hora local...??
luft_data$date<- anytime(as.factor(luft_data$date),asUTC=TRUE)
luft_data$date<- luft_data$date-8*60
# str(luft_data)
#Calcula promedios 1 hora
luft_data_h<-timeAverage(luft_data,avg.time="30 min")
assign(paste(sensor_type[i],"_",sensor[i],sep=""),luft_data_h)
# luft_data_1$date<-with_tz(luft_data_1$date,tzone="America/Argentina/Mendoza")

}
#str(luft_data_1_h)
#summary(luft_data_2_h)

```

```{r join, echo=FALSE}

#Join de particulado
luft_join<-inner_join(subset(pms7003_34021, select=c(date,PM10,PM2.5) ),subset(pms7003_34427, select=c(date,PM10,PM2.5) ),by="date",type="inner")
luft_join<-inner_join(luft_join,subset(sds011_34429, select=c(date,PM10,PM2.5) ),by="date",type="inner")
#Join con datos meteo BME 280
luft_join<-inner_join(luft_join,subset(bme280_34430, select=c(date,temperature,humidity) ),by="date",type="inner")
summaryPlot(luft_join)

```


```{r timeplot1, echo=FALSE}
#png("todosagosto.png", width = 3.5 * 300, height = 3 * 300, res = 300) 
timePlot(luft_join, pollutant = c("PM10","PM10.x","PM10.y","temperature","humidity"), y.relation = "free", smooth=FALSE, ci=95,name.pol=c("Espejo","Paso","Peatonal","temp","HR"), ylab = "pm10 (ug/m3)")

```

## Grafico con promedios de 24 horas

```{r timeplot2, echo=FALSE}
timePlot(luft_join, pollutant = c("PM10","PM10.x","PM10.y"), y.relation = "free", smooth=FALSE, ci=95,name.pol=c("Espejo","Paso","Peatonal"),group=TRUE,avg.time="day",ylab = "pm10 (ug/m3)")

```

## Grafico de la variación diaria y semanal PM10
Concentraciones sin normalizar

```{r variaciondia1, echo=FALSE}
timeVariation(luft_join, pollutant = c("PM10","PM10.x","PM10.y"),normalise="FALSE", name.pol=c("Espejo","Paso","Peatonal"),
              ylab = "PM10 (ug/m3)") 
```
